Ứng dụng Quản lý TODO
=====================

Ứng dụng này giúp bạn quản lý công việc hàng ngày với các chức năng sau:

1.  Hiển thị danh sách TODO:

    -   danh sách TODO.
2.  Xoá TODO:

    -   Cho phép bạn xoá một công việc khỏi danh sách.
3.  Đánh dấu là đã hoàn thành:

    -   Đánh dấu một công việc là đã hoàn thành.
4.  Chỉnh sửa TODO:

    -   Cung cấp khả năng chỉnh sửa thông tin của một công việc.
5.  Thêm một TODO mới:

    -   Cho phép bạn thêm một công việc mới vào danh sách.
6.  Thoát ứng dụng:

    -   Kết thúc ứng dụng và thoát khỏi giao diện người dùng.

Lưu ý:

-   Sử dụng source code từ [GitLab Repository](https://gitlab.com/huyxvd/httpclient-todo-management).
-   Sử dụng API server tại [MockAPI](https://mockapi.io/) để lưu trữ và truy xuất dữ liệu.
-   Truy cập [MockAPI](https://mockapi.io/) để đăng ký và tạo một API server mới.
-   Xác định các endpoints cần thiết để thực hiện chức năng của ứng dụng.
-   Sử dụng `HttpClient` để thực hiện yêu cầu HTTP đến API server, và đảm bảo sử dụng các phương thức `async` để thực hiện yêu cầu không đồng bộ.